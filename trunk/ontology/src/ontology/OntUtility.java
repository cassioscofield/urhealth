/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ontology;

import com.hp.hpl.jena.datatypes.RDFDatatype;
import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author lorenzo
 */
public class OntUtility {
    OntModel model;
    private String NS;
    
    
    OntUtility(OntModel model) {
        this.model=model;
        this.NS = "";
    }
    
    
    OntUtility(String filePath) {
        this.model =  ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM_RDFS_INF, null); 
        model.read(filePath, "RDF/XML-ABBREV");        
        this.NS = "";        
    }
    
    /**
     * get a OntClass from its id
     * @param id id of the the class searched
     * @return the OntClass searched
     * @throws Exception when the class is not found
     */
    public OntClass getClassFromId(String id) throws Exception{
        OntClass c = model.getOntClass(getNS()+id);
        if (c==null) {
            throw new Exception("Class not found: "+id);
        }
        return c;
    }    
    
    /**
     * return an iterator over the direct subclasses of the the class c
     * @param c dad class 
     * @return direct subclasses of c
     */
    public Iterator<OntClass> getSubClasses(OntClass c) {
        return c.listSubClasses(true);
    }
    
    /**
     * get a superclass of OntClass c
     * @param c son class
     * @return the superclass of c
     */
    public OntClass getSuperClass(OntClass c) {
        return c.getSuperClass();
    }
           
    /**
     * set a dataProperty on a class
     * @param c the class to set dataProperty onto
     * @param property the dataProperty to be set
     * @param literal the String value of the property for example "true", "false", "1.54", "6", etc
     * @param type the RDF type of the object of the dataProperty (XSDboolean, XSDfloat, etc)
     */
    public void setDataProperty(OntClass c, String property, String literal, RDFDatatype type){
        DatatypeProperty p = model.getDatatypeProperty( getNS() +property);
        c.addProperty(p,literal, type);
    }
    
    /**
     * get a value of a specific property of a class
     * @param property the property 
     * @param c the class
     * @return the value of the property of the class
     */
    public String getDataPropertyValue(String property, OntClass c){
        String value;
        try{
            value = c.getProperty(model.getDatatypeProperty(getNS()+property)).getObject().asLiteral().getString();
        }catch(Exception e){
            value = new String();
        }
        return value;        
    }
        
    /**
     * get all classes
     * @return an iterator over all classes
     */
    public Iterator<OntClass> getClasses(){
        return model.listClasses();
    }
    
    /**
     * get the current ontology model
     * @return the current OntModel
     */
    public OntModel getOntology() {
        return model;
    }
    
    /**
     * get the common name of the class
     * @param c class
     * @return the common name of the class
     */
    public String getDescription(OntClass c) {
        String description;
        try{
            description = c.getLabel("");
        }catch(Exception e){
            description = new String();
        }
        return description;        
    } 
        
    /**
     * save the ontology in a file
     * @param filePath path of the the destination file
     * @throws FileNotFoundException 
     * @throws IOException 
     */
    public void saveOntology(String filePath) throws FileNotFoundException, IOException{
        File f= new File(filePath);
        FileOutputStream fos=new FileOutputStream(f);
        model.write(fos, "RDF/XML-ABBREV");
        fos.close();
    }

    /**
     * @return the NS
     */
    public String getNS() {
        return NS;
    }

    /**
     * @param NS the NS to set
     */
    public void setNS(String NS) {
        this.NS = NS;
    }
    
    
}
