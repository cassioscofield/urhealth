/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ontology;

import com.hp.hpl.jena.datatypes.RDFDatatype;
import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * wrapper for Jena library useful on symptom ontology
 * @author lorenzo
 */
public class SymptomUtility extends OntUtility{
    
    public static final String USERVISIBILITY = "symp/src/symp.obo.owl#isUserVisible";
    public static final String MAXDURATION = "symp/src/symp.obo.owl#hasMaxDuration";
    public static final String MINDURATION = "symp/src/symp.obo.owl#hasMinDuration";
    public static final String MAXVALUE = "symp/src/symp.obo.owl#hasMaxValue";
    public static final String MINVALUE  = "symp/src/symp.obo.owl#hasMinValue";
    public static final String ISINTERMITTENT = "symp/src/symp.obo.owl#isIntermittent";
    
    SymptomUtility(OntModel model) {
        super(model);
        setNS(model.getNsPrefixURI("obo"));
    }    
    
    SymptomUtility(String filePath) {
        super(filePath);
        setNS(model.getNsPrefixURI("obo"));
    }
        
    /**
     * get the user visible classes
     * @return an iterator over the user visible classes
     */
    public Iterator<Resource> getUserVisible() {
        Property p = model.getDatatypeProperty(getNS()+USERVISIBILITY);
        Iterator<Resource> it = model.listResourcesWithProperty(p, true);
        return it;
    }
      
    /**
     * get the definition of the class, a more complete description
     * @param c class
     * @return a more complete description of the class if present
     */
    public String getDefinition(OntClass c){
        String definition;
        try{
            definition = c.getProperty(model.getOntProperty(getNS()+"IAO_0000115")).getObject().asLiteral().getString();
        }catch (Exception e){
            definition = new String();
        }
        return definition;
    }    
    
    /**
     * get the symptom with a specific word in the description or definition
     * @param word the search word
     * @return an iterator over the results symptoms
     */
    public Iterator<OntClass> searchSymptom(String word){
        ArrayList<OntClass> results = new ArrayList<OntClass>();
        Iterator<OntClass> it = this.getClasses();
        while(it.hasNext()){
            OntClass c=it.next();
            if(getDescription(c).contains(word)||getDefinition(c).contains(word)){
                results.add(c);
            }
        }
        return results.iterator();
    }
    
}
