/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ontology;

import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.ontology.OntClass;
import java.util.Iterator;

/**
 *
 * @author lorenzo
 */
public class OntologyTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception 
    {
        String pathToOntology = "file:/media/NTFS333/Dropbox/TF/Codigo/OntologyTest/symp.owl";
        String pathToOntologyOut = "/media/NTFS333/Dropbox/TF/Codigo/OntologyTest/symp.owl";
        SymptomUtility su = new SymptomUtility(pathToOntology);
        
        //print and count all classes 
        Iterator<OntClass> it=su.getClasses();
        int counter =0 ;
        while(it.hasNext()){
            System.out.println(su.getDescription(it.next()));
            counter++;
        }
        System.out.println(counter);
        System.out.println();
        
        //get class description from id
        System.out.println(su.getDescription(su.getClassFromId("SYMP_0000689")));
        System.out.println();
        
        //get direct subclasses
        Iterator<OntClass> itClasses = su.getSubClasses(su.getClassFromId("SYMP_0000462"));
        while(itClasses.hasNext()){
            System.out.println(itClasses.next().getLabel(""));
        }
        System.out.println();
        //get super class
        System.out.println(su.getDescription(su.getSuperClass(su.getClassFromId("SYMP_0000689"))));
        System.out.println();

        //get definition
        System.out.println(su.getDefinition(su.getClassFromId("SYMP_0000607")));
        System.out.println();
        
        //set visiblity=true for symtom 0000689 (cramp)
        su.setDataProperty(su.getClassFromId("SYMP_0000689"), su.USERVISIBILITY, "true", XSDDatatype.XSDboolean);        
        //print user visible symptom
        Iterator i = su.getUserVisible();
        counter =0;
        while(i.hasNext()){
            System.out.println(i.next());
            counter++;
        }
        
        //check uservisibility
        System.out.println(su.getDataPropertyValue(su.USERVISIBILITY, su.getClassFromId("SYMP_0000689")));
        
        //set and test other properties
        su.setDataProperty(su.getClassFromId("SYMP_0000689"), su.MAXDURATION, "1.16", XSDDatatype.XSDfloat);
        System.out.println(su.getDataPropertyValue(su.MAXDURATION, su.getClassFromId("SYMP_0000689")));
        
        su.setDataProperty(su.getClassFromId("SYMP_0000689"), su.MAXVALUE, "2.72", XSDDatatype.XSDfloat);
        System.out.println(su.getDataPropertyValue(su.MAXVALUE, su.getClassFromId("SYMP_0000689")));
        
        su.setDataProperty(su.getClassFromId("SYMP_0000689"), su.MINDURATION, "3.45", XSDDatatype.XSDfloat);        
        System.out.println(su.getDataPropertyValue(su.MINDURATION, su.getClassFromId("SYMP_0000689")));
        
        su.setDataProperty(su.getClassFromId("SYMP_0000689"), su.MINVALUE, "8.95", XSDDatatype.XSDfloat);
        System.out.println(su.getDataPropertyValue(su.MINVALUE, su.getClassFromId("SYMP_0000689")));
        
        su.setDataProperty(su.getClassFromId("SYMP_0000689"), su.ISINTERMITTENT, "false", XSDDatatype.XSDboolean);
        System.out.println(su.getDataPropertyValue(su.ISINTERMITTENT, su.getClassFromId("SYMP_0000689")));
        
        System.out.println();
        
        it=su.searchSymptom("cramp");
        while(it.hasNext()){
            OntClass c = it.next();
            System.out.println(su.getDescription(c) + ": " + su.getDefinition(c));
        }
        
        System.out.println();
        it=su.searchSymptom("perception");
        while(it.hasNext()){
            OntClass c = it.next();
            System.out.println(su.getDescription(c) + ": " + su.getDefinition(c));
        }
        
        //su.saveOntology(pathToOntologyOut);
        
        OntUtility du = new OntUtility("file:/media/NTFS333/Dropbox/TF/Codigo/OntologyTest/doid.owl");
        du.setNS(du.getOntology().getNsPrefixURI("obo"));
        it = du.getClasses();
        counter=0;
        while(it.hasNext()){
            System.out.println(du.getDescription(it.next()));
            counter++;
        }
        System.out.println("Total disease: "+counter);
    }
}
