package br.usp.urdoctor.android;

import br.usp.urdoctor.R;
import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class PermissoesActivity extends Activity{

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		System.out.println("Permissões");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.permissoes);
				
		setTitle();
	}
	
	public void setTitle()
	{
		TextView tv = (TextView) findViewById(R.id.textViewTitle);
		tv.setText(getText(R.string.label_permissoes));
 	}
	
	public static String getClassName()
    {
    	String className = PermissoesActivity.class.toString();
        return className.substring(6, className.length());
    }
}
