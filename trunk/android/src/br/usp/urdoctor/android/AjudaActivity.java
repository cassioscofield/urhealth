package br.usp.urdoctor.android;

import br.usp.urdoctor.R;
import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class AjudaActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		System.out.println("Ajuda");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ajuda);
				
		setTitle();
	}
	
	public void setTitle()
	{
		TextView tv = (TextView) findViewById(R.id.textViewTitle);
		tv.setText(getText(R.string.label_ajuda));
 	}
	
	public static String getClassName()
    {
    	String className = AjudaActivity.class.toString();
        return className.substring(6, className.length());
    }
}
