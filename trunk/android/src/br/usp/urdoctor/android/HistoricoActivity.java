package br.usp.urdoctor.android;

import br.usp.urdoctor.R;
import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class HistoricoActivity extends Activity
{

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		System.out.println("Histórico");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.historico);
				
		setTitle();
	}
	
	public void setTitle()
	{
		TextView tv = (TextView) findViewById(R.id.textViewTitle);
		tv.setText(getText(R.string.label_historico));
 	}
	
	public static String getClassName()
    {
    	String className = HistoricoActivity.class.toString();
        return className.substring(6, className.length());
    }
}
