package br.usp.urdoctor.android;

import br.usp.urdoctor.R;

public class Configuration {

	public static String PACKAGE_NAME = "br.usp.urdoctor";
	public static String LOGIN_ACTIVITY_NAME = PACKAGE_NAME + "android.UrDoctorActivity";
	
	static final int[] MENU_ITEMS = new int[] {
		R.string.label_sintomas,
		R.string.label_alergias,
		R.string.label_historico,
		R.string.label_permissoes,
		R.string.label_ajuda,
		R.string.label_sobre};
	
	static final int[] MENU_ICONS = new int[]{
		R.drawable.thermometer,
		R.drawable.warning,
		R.drawable.clipboard,
		R.drawable.key,
		R.drawable.ambulance,
		R.drawable.about
	};

}
