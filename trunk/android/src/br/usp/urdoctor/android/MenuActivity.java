package br.usp.urdoctor.android;

import br.usp.urdoctor.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

public class MenuActivity extends Activity 
{
	GridView gridView;
 
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		System.out.println("Menu");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu);	
		
		setGridViewListener();
		
	}
	
	public static String getClassName()
    {
    	String className = MenuActivity.class.toString();
        return className.substring(6, className.length());
    }
	
	public String[] getMenuItemAsString()
	{
		String [] items = new String[Configuration.MENU_ITEMS.length];
		
		for(int i=0;i<Configuration.MENU_ITEMS.length;i++)
		{
			items[i] = getString(Configuration.MENU_ITEMS[i]);
		}
		return items;
	}
	
	public void setGridViewListener()
	{
		gridView = (GridView) findViewById(R.id.gridViewMenu);

		gridView.setAdapter(new GridViewAdapter(this, getMenuItemAsString(),Configuration.MENU_ICONS));
 
		gridView.setOnItemClickListener(new OnItemClickListener() {
		
			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {
				// TODO	
				String item = (String) ((TextView) v.findViewById(R.id.textViewGridItemLabel)).getText();
				
				Intent i = new Intent();
				if(item.equalsIgnoreCase(getString(R.string.label_alergias)))
				{
			        i.setClassName(MenuActivity.this, AlergiasActivity.getClassName());
			        startActivity(i);
				}
				else if(item.equalsIgnoreCase(getString(R.string.label_sintomas)))
				{
					i.setClassName(MenuActivity.this, SintomasActivity.getClassName());
			        startActivity(i);
				}
				else if(item.equalsIgnoreCase(getString(R.string.label_sobre)))
				{
					i.setClassName(MenuActivity.this, SobreActivity.getClassName());
			        startActivity(i);
				}
				else if(item.equalsIgnoreCase(getString(R.string.label_ajuda)))
				{
					i.setClassName(MenuActivity.this, AjudaActivity.getClassName());
			        startActivity(i);
				}
				else if(item.equalsIgnoreCase(getString(R.string.label_permissoes)))
				{
					i.setClassName(MenuActivity.this, PermissoesActivity.getClassName());
			        startActivity(i);
				}
				else if(item.equalsIgnoreCase(getString(R.string.label_historico)))
				{
					i.setClassName(MenuActivity.this, HistoricoActivity.getClassName());
			        startActivity(i);
				}
				
				
			}
		});
	}
    
}
