package br.usp.urdoctor.android;
 
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.usp.urdoctor.R;

public class GridViewAdapter extends BaseAdapter 
{
	private Context context;
	private final String[] items;
	private final int[] icons; 
	
	public GridViewAdapter(Context context, String[] items, int[] icons) 
	{
		this.context = context;
		this.items = items;
		this.icons = icons;
	}
 
	public View getView(int position, View convertView, ViewGroup parent) {
 
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
		View gridView;
 
		if (convertView == null) {
 
			gridView = new View(context);

			// get layout from mobile.xml
			gridView = inflater.inflate(R.layout.menu_item, null);
 
			// set value into textview
			TextView textView = (TextView) gridView
					.findViewById(R.id.textViewGridItemLabel);
			textView.setText(items[position]);
 
			// set image based on selected text
			ImageView imageView = (ImageView) gridView
					.findViewById(R.id.textViewGridItemImage);
 
			try{
				imageView.setImageResource(icons[position]);	
			}
			catch(Exception e)
			{
				imageView.setImageResource(R.drawable.thermometer);
			}
 
		} else {
			gridView = (View) convertView;
		}
 
		return gridView;
	}
 
	@Override
	public int getCount() {
		return items.length;
	}
 
	@Override
	public Object getItem(int position) {
		return null;
	}
 
	@Override
	public long getItemId(int position) {
		return 0;
	}
 
}