package br.usp.urdoctor.android;

import br.usp.urdoctor.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

public class UrDoctorActivity extends Activity 
{
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
    	System.out.println("Login");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        
        Button loginButton = (Button) findViewById(R.id.buttonLogin);
        loginButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) 
			{
				// Chama nova tela
				Intent i = new Intent();
		        i.setClassName(UrDoctorActivity.this, MenuActivity.getClassName());
				startActivity(i);
			}
		});
    }
    
    public static String getClassName()
    {
    	String className = UrDoctorActivity.class.toString();
        return className.substring(6, className.length());
    }
    
}