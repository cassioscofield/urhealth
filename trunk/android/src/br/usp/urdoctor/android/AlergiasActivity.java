package br.usp.urdoctor.android;

import br.usp.urdoctor.R;
import android.app.Activity;
import android.os.Bundle;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

public class AlergiasActivity extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		System.out.println("Alergias");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.alergias);
		
		ExpandableListView listView =(ExpandableListView)findViewById(R.id.expandableListViewAlergias);
		
		String[] groups = this.getResources().getStringArray(R.array.alergias);
		
		String[] medicamentos = getResources().getStringArray(R.array.alergia_medicamentos);
		String[] alimentos = getResources().getStringArray(R.array.alergia_alimentos);
		String[] vacinas = {getResources().getString(R.string.label_especificar)};
		String[] pele = getResources().getStringArray(R.array.alergia_pele);
		
		String[][] children = new String[][] { medicamentos, alimentos, vacinas, pele};
		
		BaseExpandableListAdapter mAdapter = new ExpandableListViewAdapter(groups,children,this);
		listView.setAdapter(mAdapter);
		
		setTitle();
	}
	
	public void setTitle()
	{
		TextView tv = (TextView) findViewById(R.id.textViewTitle);
		tv.setText(getText(R.string.label_alergias));
 	}
	
	public static String getClassName()
    {
    	String className = AlergiasActivity.class.toString();
        return className.substring(6, className.length());
    }
}
